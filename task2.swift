let numberOfDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let monthNames  = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
let amount = 12

for numbers in numberOfDays{
    print(numbers)
}

print()

var index = 0
while index < amount{
print("\(numberOfDays[index]) days in \(monthNames[index])")
    index = index + 1;
}

print()

var tuples : Array<(String, Int)> = []
index = 0
while index < amount{
    tuples.append((monthNames[index], numberOfDays[index]))
    index = index + 1
}

for (month, days) in tuples{
    print("\(days) days in \(month)")
}

print()

index = amount - 1;

while index >= 0{
    print("\(tuples[index].1) days in \(tuples[index].0)")
    index = index - 1
}

print()

let month = "October"
let day = 3
var count = day - 1

index = 0
repeat {

    count = count + tuples[index].1
    index = index + 1
    
} while tuples[index].0 != month
print("There will be \(count) days from the beginning of the year to \(day) \(month).")